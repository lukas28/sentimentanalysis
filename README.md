# SentimentAnalysis

This project provides the steps for reading twitter tweets labeled with the given sentiment categorized by humans. This data is further preprocessed and used to train a neural network to do the sentiment analysis later on.

The project covers the following tasks:

1. Reading in the twitter sentiment analysis dataset
2. Getting rid of unusable symbols and converting human readable text to integer sequences
3. One-hot encoding the sentiments
4. Creating a Sequental GRU model (Gated Recurrent Unit). For further information lookup this paper: https://arxiv.org/abs/1412.3555
5. Training the model and provide a method to evaluate

The dataset used in this project can be downloaded via: http://cs.stanford.edu/people/alecmgo/trainingandtestdata.zip

## How-To

To use the code, go ahead and download the dataset from the link provided above. Rename the csv file intended for training to training.csv and move it 
into the project folder.
Now open the Terminal and navigate to that folder.
To train the network, use the command:
`python3 BinarySentimentAnalysis.py`
This will create start the training process and eventually produce the model.h5 file after the first epoch of training is done.