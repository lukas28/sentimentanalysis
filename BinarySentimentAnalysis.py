'''
Importing the required libraries
'''
import nltk
from nltk.stem import WordNetLemmatizer
import numpy as np
nltk.download('punkt')
nltk.download('wordnet')
import pandas as pd
import regex as re
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from keras.models import Sequential
from keras.layers import Bidirectional, Dense, Dropout, Embedding, BatchNormalization, GRU
from keras.callbacks import ModelCheckpoint


'''
Method to load the csv file from the drive, pick out the text and sentiment and return them as a tupel of lists
'''
def load_csv_file(filename):
    dataframe = pd.read_csv(filename, encoding = 'latin1', names = ['sentiment', 'id', 'date', 'queryClass', 'author', 'text'])
    sentiment = dataframe['sentiment']
    text = dataframe['text']
    return (text.tolist(), sentiment.tolist())

'''
Method used to get rid of unnecessary symbols only leaving characters and numbers and also use the lemmatizer to
kind of normalize words. For better understand what the lemmatizer really does look up: https://en.wikipedia.org/wiki/Lemmatisation
'''
def removeUnneccessarySymbols(sentence):
    lemmatizer = WordNetLemmatizer()
    clearWords = []
    for symbol in sentence:
        clean = re.sub(r'[^ a-z A-Z 0-9]', " ", symbol)
        w = nltk.word_tokenize(clean)
        clearWords.append([lemmatizer.lemmatize(i.lower()) for i in w])
    return clearWords

'''
Method creates a tokenizer object fitting to the given text. The basic idea behind a tokenizer is to convert words
into interger representations. For better understanding lookup Keras' documentation: https://keras.io/preprocessing/text/
The fit_on_texts method within the Tokenizer class basically creates a dictionary which holds the word frequency within 
the given texts. word --> occured x times
'''
def getTokenizer(words, filters = '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~'):
    token = Tokenizer(filters = filters)
    token.fit_on_texts(words)
    return token

'''
Method returns the length of the longest word within the words
'''
def getLongestWord(words):
    return len(max(words, key = len))

'''
This method takes the Tokenizer object and the words from the text and does the converstion from human readably
words to intergers. The text_to_squences method performs the mapping given by the token dictionary created by the
fit_on_texts method above.
'''
def encodeWords(token, words):
    return token.texts_to_sequences(words)

'''
This method is used to pad shorter sentences to the length of the longest sentence. This is important because later on
the neural network expects the input (sentences) to be of equal length.
'''
def padWordsToMaxLen(words, maxLen):
    return pad_sequences(words, maxlen = maxLen, padding = 'post')

'''
Within the used dataset, the sentiment code for negative purpose is 0 and for positive purpose is 4. This method
is used to encode negatives as [0,1] and positives as [1,0]. This is also called One-Hot-Encoding.
'''
def encodeSentiments(sentiments):
    oneHotEncodedSentiments = []
    for sentiment in sentiments:
        if sentiment == 0:
            oneHotEncodedSentiments.append([0,1])
        else:
            oneHotEncodedSentiments.append([1,0])
    return np.array(oneHotEncodedSentiments)

'''
This method creates the sequential model for our sentiment analysis
'''
def nlpModel(wordCount, maxLen):
    model = Sequential()
    model.add(Embedding(wordCount, 128, input_length=maxLen, trainable=False))
    model.add(Bidirectional(GRU(128)))
    model.add(Dense(64, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation="relu"))
    model.add(Dropout(0.5))
    model.add(BatchNormalization())
    model.add(Dense(2, activation="softmax"))

    return model

def predictions(text, model, expectedModelInputLength):
    clean = re.sub(r'[^ a-z A-Z 0-9]', " ", text)
    test_word = nltk.word_atokenize(clean)
    lemmatizer = WordNetLemmatizer()
    test_word = [lemmatizer.lemmatize(w.lower()) for w in test_word]
    test_ls = nltk.word_tokenizer.texts_to_sequences(test_word)

    # Check for unknown words
    if [] in test_ls:
        test_ls = list(filter(None, test_ls))

    test_ls = np.array(test_ls).reshape(1, len(test_ls))

    x = padWordsToMaxLen(test_ls, expectedModelInputLength)

    pred = model.predict_classes(x)
    return pred


def main():
    # loading the csv file
    text, sentiment = load_csv_file('training.csv')

    # clean texts
    words = removeUnneccessarySymbols(text)

    # create a tokenizer for the texts
    tokenizer = getTokenizer(words)

    # encode text
    tokens = encodeWords(tokenizer, words)

    # gets the number of different words occured in the texts
    wordCount = len(tokenizer.word_index) + 1

    # get the length of the longest word
    maxLen = getLongestWord(tokens)

    # pad out the tokens (words) so all inputs are of same length
    paddedTokens = padWordsToMaxLen(tokens, getLongestWord(tokens))

    # one hot encode the senitments (negative or positive)
    encodedSentiments = encodeSentiments(sentiment)

    # Do the test and train split --> 80% train and 20% test
    train_X, eval_X, train_Y, eval_Y = train_test_split(paddedTokens, encodedSentiments, shuffle=True, test_size=0.2)

    # create, compile and print out the model
    model = nlpModel(wordCount, getLongestWord(tokens), maxLen)
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    model.summary()

    # define a checkpoint saving the best epoch as h5 model
    filename = 'model.h5'
    checkpoint = ModelCheckpoint(filename, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

    # train the model and save the progress in the hist variable
    hist = model.fit(train_X, train_Y, epochs=100, batch_size=32, validation_data=(eval_X, eval_Y),
                     callbacks=[checkpoint])


if __name__ == "__main__":
    main()





